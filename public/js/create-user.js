$('form.create-user').on('submit', function(e) {

	var form   = $(this);
	var method = form.attr("method");
	var url    = form.attr("action");
	var data   = form.serialize();

	$.ajax({
		url    : url,
		method : method,
		data   : data,
		success : function(response){

			if (response.success == true) {

				$('.response').text('User successfully saved');
				
			}
			else {

				var error_string = '<ul>';
				$.each( response.errors, function( key, value) {
				  error_string += '<li>' + value + '</li>';
				});
				error_string += '</ul>';

				$('.response').html(error_string);

			}

		}
	});

	return false;

});
