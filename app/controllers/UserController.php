<?php

class UserController extends \BaseController {


	public function getCreate() {
	
		return View::make('user-create');
	}

	public function postCreate(){

		$inputData = Input::all('formData');

		$userData = [
			'forname' => $inputData['firstname'],
			'surname'  => $inputData['lastname'],
			'username' => $inputData['username'],
		];

		$validator = Validator::make($userData, User::$rules);

		if($validator->fails()) {
			return Response::json([
				'success' => false,
				'errors'  => $validator->getMessageBag()->toArray()
			]);
		}
		else {

			$User = User::create($userData);
            return Response::json(['success' => true, 'message' => 'User successfully created']);

		}
	}

}
