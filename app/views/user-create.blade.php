<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Create User</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    
    <form class="form-horizontal create-user" method="POST" action="create-user">
    <fieldset>

    <!-- Form Name -->
    <legend>Create User</legend>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="firstname">First name</label>  
      <div class="col-md-4">
      <input id="firstname" name="firstname" type="text" placeholder="e.g. John" class="form-control input-md" required="">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="lastname">Last name</label>  
      <div class="col-md-4">
      <input id="lastname" name="lastname" type="text" placeholder="e.g. Appleseed" class="form-control input-md" required="">
        
      </div>
    </div>

    <!-- Text input-->
    <div class="form-group">
      <label class="col-md-4 control-label" for="username">Username</label>  
      <div class="col-md-4">
      <input id="username" name="username" type="text" placeholder="e.g. jonny" class="form-control input-md" required="">
        
      </div>
    </div>

    <!-- Button -->
    <div class="form-group">
      <label class="col-md-4 control-label" for="singlebutton"></label>
      <div class="col-md-4">
        <button type="submit" id="singlebutton" name="singlebutton" class="btn btn-success" value="submit">Save</button>
      </div>
    </div>

    <div class="response"></div>


    </fieldset>
    </form>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/create-user.js"></script>


    <script>

   


    </script>

  </body>
</html>
