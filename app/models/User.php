<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

	use UserTrait;

	protected $table      = 'users';
	protected $primaryKey = 'id';
	protected $hidden     = ['password'];

	public static $rules = [
		'username' => 'required|unique:users|between:2,50',
		'forname'  => 'required|between:2,50',
		'surname'  => 'required|between:2,50',
	];

	protected $fillable = [
		'username',
		'forname',
		'surname',
	];

}
