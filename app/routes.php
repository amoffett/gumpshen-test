<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Route all
Route::get('/', function()	{
    return Redirect::to('create-user');
});


Route::get('create-user', ['uses' => 'UserController@getCreate']);
Route::post('create-user', ['uses' => 'UserController@postCreate']);
